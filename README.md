# Race Game Client

## 圖片設定黨修改或新增

	js/config.js

		defaultValue : {
			color: {
				'black' : '黑色' // key: value
			}
		}

	images/black_horse.png //key_horsekit.png

	目前附檔名都只吃png

	Example:

	defaultValue: {
		color: {
			'black': '黑色',
			'yellow': '黃色'
		}
	}

	將圖檔放置在此處並依照規則命名

	images/black_horse.png // 對應黑色

	image/yellow_horse.png // 對應黃色
