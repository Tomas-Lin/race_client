'use strict';
var socket = io(defaultConfig.WSServer + ":" + defaultConfig.WSPort);
var user = {};

socket.on('/newRoom', function(data){
	var room = _.pick(data, "id", "status", "complete", "users", "startTime", "endTime", "max");
	var config = getConfig();
	config.room = room;
	save(config);
	$("#canInvite").html(room.max);
	$("#qrcode").attr("src", data.qrcode);
//    window.location.href = "/room.html?roomID=" + data.roomID;
});

socket.on('/clientAddNewUser', function(data){
	alert("歡迎" + data.nickname + "加入遊戲");

});

socket.on('/addRoomSuccess', function(data){
	$("#form").addClass("hide");
	$("#userReady").removeClass("hide");
	$("#goalCount").html(data.goalCount);
	save(data.user, 'self');
	alert("加入遊戲成功");
});

socket.on('/startGame', function(data){
	var roomInfo = getRoomID();
	if(data.roomID === roomInfo.roomID){
		$("#gameBlock").removeClass("hide");
		$("#userReady").addClass("hide");
		var room = Object.assign(roomInfo, data);
		save(room , 'roomData');
		var diff = moment(room.startTime).diff(moment());
		countDown([(diff/1000), 'seconds'], 'countdown');
	}
});

socket.on('/newStatus', function(data){
	var room = getRoomID();
	if(room.roomID === data.roomID){
		user = data.user;
		$("#status").html(user.progress);
		if(data.user.complete){
			var diff = moment(data.user.endTime).diff(room.startTime);
			$("#resultBlock").removeClass("hide");
			$("#result").html(data.user.place);
			$("#spend").html( (diff/1000) + "秒");
		}
	}
});

socket.on('/leaveRoom', function(data){
	var room = getRoomID();
	if(data.roomID === room.roomID){
		$("#leaveMsg").removeClass("hide");
		$(".label-danger").html(data.nickname + "離開了房間");

		setTimeout(function(){
			$("#leaveMsg").addClass("hide");
		}, 2000);
	}
});

socket.on('/Error', function(data){
    if(defaultConfig.dev){
        alert(data.message);
    }
});
