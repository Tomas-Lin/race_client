var defaultConfig = {
    dev: true,
    WSServer: '45.79.143.70',
    WSPort: 3000,
	readyForInvite : [10, "minutes"],
	playerMax : 10,
	WNServer: "http://localhost:3000",
	shakeConf: {
		threshold: 10, // optional shake strength threshold
		timeout: 50 // optional, determines the frequency of event generation
	},
	color:
		{
			black : "黑色",
		},
}
