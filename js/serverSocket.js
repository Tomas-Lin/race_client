'use strict';

var socket = io("http://45.79.143.70:3000");

socket.on('/newRoom', function(data){
	var room = _.pick(data, "id", "status", "complete", "users", "startTime", "endTime", "max", "goalCount");
	var roomInfo = {
		roomID: room.id,
		status : room.status,
		goalCount : room.goalCount,
		max: room.max,
		startTime: room.startTime,
	};
	var config = getConfig();
	config.room = room;
	save(config);
	save(roomInfo, 'roomData');
	$("#canInvite").html(room.max);
	$("#qrcode").attr("src", data.qrcode);
//    window.location.href = "/room.html?roomID=" + data.roomID;
});

socket.on('/serverAddNewUser', function(data){
	$("#canInvite").html(data.caninvited);
	$("#invited").html(data.invited);
});

socket.on('/newStatus', function(data){
		var tagID = compareID(data.user.id);
		updateLocalUser(data.user);
		var roomInfo = getRoomID();
		if(data.roomID === roomInfo.roomID){
			var progress = (data.user.progress / roomInfo.goalCount) * 100;
			$("#" + tagID).css("width", progress + '%');
			if(data.user.complete){
				$("#" + tagID).css("background-color",'#CCCCCC');
				var status = checkUsersStatus(roomInfo);
				if(status){
					location.href = "/server/result.html";
				}
			}
		}
});

socket.on('/startGame', function(data){
	var roomInfo = getRoomID();
	if(data.roomID === roomInfo.roomID){
		var room = Object.assign(roomInfo, data);
		save(room, 'roomData');
		location.href = '/server/game.html';
	}
});

socket.on('/leaveRoom', function(data){
	
	var room = getRoomID();
	if(data.roomID === room.roomID){
		$("#leaveMsg").removeClass("hide");
		$(".label-danger").html(data.nickname + "離開了房間");

		setTimeout(function(){
			$("#leaveMsg").addClass("hide");
		}, 2000);
	}
});

socket.on('/serverError', function(data){
	alert(data.message);
});
