
$(function(){
	var myShakeEvent = new Shake(defaultConfig.shakeConf);

	myShakeEvent.start();

	window.addEventListener('shake', shakeEventDidOccur, false);
		function shakeEventDidOccur () {
			if(window.canShake){
				var room = getRoomID();
				socket.emit('/run', room);
			}

		}
});
