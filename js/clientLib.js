'use strict'


function checkPhoto(){
	var color = $("#color").val();
	if(!_.isEmpty(color)){
		$("#preview").attr("src", "/images/" + color + "_horse.png");
	}else{
		$("#preview".attr("src", ""));
	}
}

//開始準備遊戲
function createRoom(){
	socket.emit("/createRoom");
	var config = getConfig();
	countDown(config.readyForInvite, 'clock');
	$("#canInvite").html(config.playerMax);
	$("#ready")
		.html('<p class="text-danger">掃描QRCode 進入房間</p>');
	$(".hide").removeClass("hide").addClass('show');
}

function save(data, key){
	var key = key || 'config';
	localStorage.setItem(key, JSON.stringify(data));
}

function getConfig(){
	return JSON.parse(localStorage.getItem("config"));
}

function getRoomID(){
	return JSON.parse(localStorage.getItem("roomData"));
}

function getSelf(){
	return JSON.parse(localStorage.getItem("self"));
}

//倒數計時
function countDown(time, tagID){
	var date = moment().add(time[0], time[1]);
   var timerId = countdown(date,function(ts) {
	   if(ts.value > 0){
		   window.canShake = true;
			window.clearInterval(timerId);
	   }else{
			document.getElementById(tagID).innerHTML = ts.toHTML("strong");
	   }
	});
}

function invitedGame(){
	var roomData = getRoomID();
	var data = {
		nickname : $("#nickname").val(),
		color : $("#color").val(),
		roomID: roomData.roomID
	};
	if(_.isEmpty(data.nickname)){
		alert("暱稱不可為空");
	}else if(_.isEmpty(data.color)){
		alert("請選擇馬匹顏色");
	}else{
		socket.emit("/invited", data);
	}

}
