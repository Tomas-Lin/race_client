'use strict';

//遊戲計時用
var gameStartCount;

//顯示比賽結果
function displayResult(){
	var room = getRoomID();
	var users = _.sortBy(room.users, function(user){
		return user.place;
	});
	var html = '';
	_.map(users, function(user){
		var diff = moment(moment(room.startTime).diff(user.endTime));
		html = html + '<div class="row"> \
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2 col-lg-offset-2 col-md-offset-2"> \
							<image style="widht:100px;height:100px;" src="/images/' + user.color + '_horse.png"> \
						</div> \
						<div class="col-xs-12 col-sm-12 col-md-7 col-lg-8"> \
							<p class="Result-User">1. ' +  user.nickname + ' : ' + diff.minutes()+ ':' + diff.seconds() + '</p> \
						</div> \
					  </div>';
	});
	$("#result").html(html);
}

//設定遊戲開始時間
function setStartTime(){
	var roomInfo = getRoomID();
	gameStartCount(roomInfo.startTime, 'timer');
}

//遊戲結束
function goToResult(){
	window.clearInterval(gameStartCount);

}

function updateLocalUser(user){
	var room = getRoomID();
	var index = _.findIndex(room.users, function(u){
		return u.id === user.id
	});
	room.users[index] = user;
	save(room, 'roomData');
}

function checkUsersStatus(room){
	var goalusers = _.filter(room.users, function(u){
		return u.complete === true;
	});
	if(goalusers.length === room.users.length) return true;
	else return false
}

//Server 列表
function displayUsers(){
	var roomInfo = getRoomID();
	roomInfo.users.map(function(user, index){
		var tagID = compareID(user.id);
		$("#gameBlock").append('<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"> \
			<p class="Race-User">' + ( index + 1) + '. ' + user.nickname + ' </p> \
		</div> \
	  <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10"> \
		  <div  id=' + tagID + ' class="Road_left" style="width:0%;float:left;"></div> \
		  <image style="width: 50px;height:50px;" src="/images/' + user.color + '_horse.png"> \
		</div> \
	</div>');
	});
}

function checkPhoto(){
	var color = $("#color").val();
	if(!_.isEmpty(color)){
		$("#preview").attr("src", "/images/" + color + "_horse.png");
	}else{
		$("#preview".attr("src", ""));
	}
}

function startGame(){
	var roomInfo = getRoomID();
	socket.emit("/startGame", roomInfo);
}

//開始準備遊戲
function createRoom(){
	socket.emit("/createRoom");
	var config = getConfig();
	countDown(config.readyForInvite, 'clock');
	$("#canInvite").html(config.playerMax);
	$("#ready")
		.html('<p class="text-danger">掃描QRCode 進入房間</p>');
	$(".hide").removeClass("hide").addClass('show');
}

function save(data, key){
	var key = key || 'config';
	localStorage.setItem(key, JSON.stringify(data));
}

function getConfig(){
	return JSON.parse(localStorage.getItem("config"));
}

function getRoomID(){
	return JSON.parse(localStorage.getItem("roomData"));
}
//遊戲計時
function gameStartCount(time, tagID){
	var date = moment(time);
	gameStartCount = countdown(date, function(ts){
		document.getElementById(tagID).innerHTML = ts.toHTML("strong");
		if(ts.value > 0){
			//遊戲開始
		}
	});
}

//倒數計時
function countDown(time, tagID){
	var date = moment().add(time[0], time[1]);
   var timerId = countdown(date,function(ts) {
	   if(ts.value > 0){
		   startGame();
			window.clearInterval(timerId);
	   }else{
			document.getElementById(tagID).innerHTML = ts.toHTML("strong");
	   }
	});
}

function invitedGame(){
	var roomData = getRoomID();
	var data = {
		nickname : $("#nickname").val(),
		color : $("#color").val(),
		roomID: roomData.roomID
	};
	if(_.isEmpty(data.nickname)){
		alert("暱稱不可為空");
	}else if(_.isEmpty(data.color)){
		alert("請選擇馬匹顏色");
	}else{
		socket.emit("/invited", data);
	}
}

function compareID(id){
	return id.replace(/[+|=|\/|\#|\-|]/g, '');
}
